# Le Dang Family Website
> last updated: Sep 15, 2017

The inspiration for this website is to create a platform
to connect the family members as well as serve as an 
educational tool for the next generation.

# Features
- The family tree with clickable member link
- Foundational document

## goals
- Create an responsive webpage that can be viewed on multiple
platform (desktop, mobile)
- Should contain historical and educational information about
the family and the country with significant events
- Should be interactive and captivating (animation)

## Planned features
- Timeline of events
- Information about the country when click on the link
