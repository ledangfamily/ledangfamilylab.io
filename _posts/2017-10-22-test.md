---
title: Family quotes
layout: individualArticle
excerpt: Beautiful quotes about family from brainyquote.com
url: _posts/2017-10-22.md
---

Learn to enjoy every minute of your life. Be happy now. Don't wait for something outside of yourself to make you happy in the future. Think how really precious is the time you have to spend, whether it's at work or with your family. Every minute should be enjoyed and savored.
> Earl Nightingale

Happiness is having a large, loving, caring, close-knit family in another city.
> George Burns

The bond that links your true family is not one of blood, but of respect and joy in each other's life.
> Richard Bach

The bond that links your true family is not one of blood, but of respect and joy in each other's life.
> Esha Gupta Read more at: https://www.brainyquote.com/quotes/quotes/e/eshagupta579875.html?src=t_family