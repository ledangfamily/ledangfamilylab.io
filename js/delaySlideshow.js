/**
 * Delays project cards animation
 * and removes the default 'active' link
 * when clicked
 */
$(document).ready(()=>{
    $(".mainSlideshow").hide();
    $(".mainSlideshow").slideDown(3000);
})